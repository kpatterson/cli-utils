/*
 * CmdLineParamParser.cpp
 * TruEd LMS - A web-based learning management system for small schools
 *
 *       Created on: September 7, 2016
 *   Primary Author: Kevin H. Patterson
 *     Contributors: N/A
 *
 * Copyright © 2015-2017 Hartland Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * See the LICENSE file for detailed terms of use.
 */

#include "CmdLineParamParser.hpp"


CmdLineParamParser::CmdLineParamParser( int argc, const char* argv[], size_t i_FirstParamIndex )
{
	for( int i = int( i_FirstParamIndex ); i < argc; ++i ) {
		m_Params.push_back( argv[i] );
	}

	for( size_t t_Index = 0; t_Index < m_Params.size(); ++t_Index )
		m_ParamIndex.insert( { m_Params[t_Index], t_Index } );
}


CmdLineParamParser::CmdLineParamParser( const std::vector<std::string>& i_Params, size_t i_FirstParamIndex )
{
	std::copy( i_Params.begin() + i_FirstParamIndex, i_Params.end(), std::back_inserter( m_Params ) );
	for( size_t t_Index = 0; t_Index < m_Params.size(); ++t_Index )
		m_ParamIndex.insert( { m_Params[t_Index], t_Index } );
}

std::string CmdLineParamParser::GetParamValue( const std::string& i_Param ) const {
	auto it = m_ParamIndex.find( i_Param );
	if( it != m_ParamIndex.end() ) {
		size_t t_Index = it->second + 1;
		if( t_Index < m_Params.size() )
			return m_Params[t_Index];
	}
	return std::string();
}


bool CmdLineParamParser::HasParam( const std::string& i_Param ) const {
	auto it = m_ParamIndex.find( i_Param );
	return it != m_ParamIndex.end();
}
