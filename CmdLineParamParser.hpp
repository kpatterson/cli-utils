/*
 * CmdLineParamParser.hpp
 * TruEd LMS - A web-based learning management system for small schools
 *
 *       Created on: September 7, 2016
 *   Primary Author: Kevin H. Patterson
 *     Contributors: N/A
 *
 * Copyright © 2015-2017 Hartland Institute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * See the LICENSE file for detailed terms of use.
 */

#ifndef CmdLineParamParser_hpp
#define CmdLineParamParser_hpp

#include <string>
#include <map>
#include <vector>

typedef std::map<std::string,int> CmdLineParamMap_t;


class CmdLineParamParser {
public:
	CmdLineParamParser( int argc, const char* argv[], size_t i_FirstParamIndex = 1 );

	CmdLineParamParser( const std::vector<std::string>& i_Params, size_t i_FirstParamIndex = 1 );

	virtual ~CmdLineParamParser() {}
	
	std::string GetParamValue( const std::string& i_Param ) const;

	bool HasParam( const std::string& i_Param ) const;
	
private:
	std::vector<std::string> m_Params;
	CmdLineParamMap_t m_ParamIndex;
};

#endif /* CmdLineParamParser_hpp */
